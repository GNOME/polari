# Italian translation of polari
# Copyright (C) 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020 Free Software Foundation, Inc.
# This file is distributed under the same license as the polari package.
# Milo Casagrande <milo@milo.name>, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020.
# Gianvito Cavasoli <gianvito@gmx.it>, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: polari\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/polari/issues\n"
"POT-Creation-Date: 2020-06-08 18:31+0000\n"
"PO-Revision-Date: 2020-08-27 09:59+0200\n"
"Last-Translator: Milo Casagrande <milo@milo.name>\n"
"Language-Team: Italiano <gnome-it-list@gnome.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Launchpad-Export-Date: 2011-04-18 19:13+0000\n"
"X-Generator: Poedit 2.4.1\n"

#: data/appdata/org.gnome.Polari.appdata.xml.in:7
#: data/org.gnome.Polari.desktop.in:3 data/resources/main-window.ui:39
#: src/roomStack.js:207
msgid "Polari"
msgstr "Polari"

#: data/appdata/org.gnome.Polari.appdata.xml.in:8
#: data/org.gnome.Polari.desktop.in:4 src/application.js:841
msgid "An Internet Relay Chat Client for GNOME"
msgstr "Un programma IRC per GNOME"

#: data/appdata/org.gnome.Polari.appdata.xml.in:10
msgid ""
"A simple Internet Relay Chat (IRC) client that is designed to integrate "
"seamlessly with GNOME; it features a simple and beautiful interface which "
"allows you to focus on your conversations."
msgstr ""
"Un client IRC (Internet Relay Chat) semplice e progettato per integrarsi con "
"GNOME: un'interfaccia grafica esteticamente appagante che consente di "
"concentrarsi sulle proprie conversazioni."

#: data/appdata/org.gnome.Polari.appdata.xml.in:15
msgid ""
"You can use Polari to publicly chat with people in a channel, and to have "
"private one-to-one conversations. Notifications make sure that you never "
"miss an important message — for private conversations, they even allow you "
"to reply instantly without switching back to the application!"
msgstr ""
"È possibile usare Polari per chiacchierare con altre persone in un canale e "
"avere conversazioni private. Le notifiche consentono di non perdere mai "
"alcun messaggio importante: nelle conversazioni private è possibile "
"rispondere immediatamente dalla notifica senza dover usare direttamente "
"l'applicazione."

#: data/appdata/org.gnome.Polari.appdata.xml.in:48
msgid "The GNOME Project"
msgstr "Il progetto GNOME"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.Polari.desktop.in:15
msgid "IRC;Internet;Relay;Chat;"
msgstr "IRC;Internet;Chat;Conversazioni;"

#: data/org.gnome.Polari.gschema.xml:6
msgid "Saved channel list"
msgstr "Elenco canali salvati"

#: data/org.gnome.Polari.gschema.xml:7
msgid "List of channels to restore on startup"
msgstr "Elenco di canali da ripristinare all'avvio"

#: data/org.gnome.Polari.gschema.xml:11 data/resources/main-window.ui:16
msgid "Run in Background"
msgstr "Esegui in background"

#: data/org.gnome.Polari.gschema.xml:12
msgid "Keep running in background when closed."
msgstr "Continua l'esecuzione anche dopo aver chiuso l'applicazione."

#: data/org.gnome.Polari.gschema.xml:16
msgid "Window size"
msgstr "Dimensione finestra"

#: data/org.gnome.Polari.gschema.xml:17
msgid "Window size (width and height)."
msgstr "Dimensione finestra (larghezza e altezza)."

#: data/org.gnome.Polari.gschema.xml:21
msgid "Window maximized"
msgstr "Finestra massimizzata"

#: data/org.gnome.Polari.gschema.xml:22
msgid "Window maximized state"
msgstr "Lo stato massimizzato della finestra"

#: data/org.gnome.Polari.gschema.xml:26
msgid "Last active channel"
msgstr "Ultimo canale attivo"

#: data/org.gnome.Polari.gschema.xml:27
msgid "Last active (selected) channel"
msgstr "Ultimo canale attivo (selezionato)"

#: data/org.gnome.Polari.gschema.xml:34
msgid "Identify botname"
msgstr "Bot d'identificazione"

#: data/org.gnome.Polari.gschema.xml:35
msgid "Nickname of the bot to identify with"
msgstr "Soprannome del bot con cui identificarsi"

#: data/org.gnome.Polari.gschema.xml:39
msgid "Identify command"
msgstr "Comando d'identificazione"

#: data/org.gnome.Polari.gschema.xml:40
msgid "Command used to identify with bot"
msgstr "Comando usato per identificarsi col bot"

#: data/org.gnome.Polari.gschema.xml:44
msgid "Identify username"
msgstr "Nome utente d'identificazione"

#: data/org.gnome.Polari.gschema.xml:45
msgid "Username to use in identify command"
msgstr "Nome utente da usare con il comando di identificazione"

#: data/org.gnome.Polari.gschema.xml:49
msgid "Identify username supported"
msgstr "Nome utente d'identificazione supportato"

#: data/org.gnome.Polari.gschema.xml:50
msgid "Whether the identify command is known to support the username parameter"
msgstr ""
"Indica se il comando di identificazione è conosciuto per supportare il "
"parametro nome utente"

#: data/org.gnome.Polari.gschema.xml:56
msgid "List of muted usernames"
msgstr "Elenco di utenti silenziati"

#: data/org.gnome.Polari.gschema.xml:57
msgid ""
"A list of usernames for whose private messages not to show notifications"
msgstr "Un elenco di nomi utente i cui messaggi privati non generano notifiche"

#: data/resources/connection-details.ui:13
msgid "_Server Address"
msgstr "Indirizzo _server"

#: data/resources/connection-details.ui:33
msgid "Net_work Name"
msgstr "Nome _rete"

#: data/resources/connection-details.ui:49
#: data/resources/connection-details.ui:116
msgid "optional"
msgstr "opzionale"

#: data/resources/connection-details.ui:61
msgid "Use secure c_onnection"
msgstr "Usare connessione _sicura"

#: data/resources/connection-details.ui:76
msgid "_Nickname"
msgstr "Sopran_nome"

#: data/resources/connection-details.ui:101
msgid "_Real Name"
msgstr "Nome _reale"

#: data/resources/connection-properties.ui:9 data/resources/entry-area.ui:110
#: data/resources/join-room-dialog.ui:11 src/initialSetup.js:85
msgid "_Cancel"
msgstr "A_nnulla"

#: data/resources/connection-properties.ui:17
msgid "_Apply"
msgstr "A_pplica"

#: data/resources/entry-area.ui:38
msgid "Change nickname"
msgstr "Cambia soprannome"

#: data/resources/entry-area.ui:120
msgid "_Paste"
msgstr "_Incolla"

#: data/resources/help-overlay.ui:15
msgctxt "shortcut window"
msgid "General"
msgstr "Generale"

#: data/resources/help-overlay.ui:19
msgctxt "shortcut window"
msgid "Join Room"
msgstr "Entra in una stanza"

#: data/resources/help-overlay.ui:27
msgctxt "shortcut window"
msgid "Leave Room"
msgstr "Esce dalla stanza"

#: data/resources/help-overlay.ui:34
msgctxt "shortcut window"
msgid "Show Userlist"
msgstr "Mostra l'elenco utenti"

#: data/resources/help-overlay.ui:41
msgctxt "shortcut window"
msgid "Show Emoji Picker"
msgstr "Mostra selettore emoji"

#: data/resources/help-overlay.ui:48
msgctxt "shortcut window"
msgid "Show Help"
msgstr "Mostra l'aiuto"

#: data/resources/help-overlay.ui:55
msgctxt "shortcut window"
msgid "Open Menu"
msgstr "Apre il menù"

#: data/resources/help-overlay.ui:62
msgctxt "shortcut window"
msgid "Quit"
msgstr "Esce"

#: data/resources/help-overlay.ui:69
msgctxt "shortcut window"
msgid "Keyboard Shortcuts"
msgstr "Scorciatoie da tastiera"

#: data/resources/help-overlay.ui:78
msgctxt "shortcut window"
msgid "Navigation"
msgstr "Navigazione"

#: data/resources/help-overlay.ui:82
msgctxt "shortcut window"
msgid "Next Room"
msgstr "Stanza successiva"

#: data/resources/help-overlay.ui:89
msgctxt "shortcut window"
msgid "Previous Room"
msgstr "Stanza precedente"

#: data/resources/help-overlay.ui:96
msgctxt "shortcut window"
msgid "Next Room with Unread Messages"
msgstr "Stanza successiva con messaggi non letti"

#: data/resources/help-overlay.ui:103
msgctxt "shortcut window"
msgid "Previous Room with Unread Messages"
msgstr "Stanza precedente con messaggi non letti"

#: data/resources/help-overlay.ui:110
msgctxt "shortcut window"
msgid "First Room"
msgstr "Prima stanza"

#: data/resources/help-overlay.ui:117
msgctxt "shortcut window"
msgid "Last Room"
msgstr "Ultima stanza"

#: data/resources/help-overlay.ui:124
msgctxt "shortcut window"
msgid "First – Ninth Room"
msgstr "Prima - Nona stanza"

#: data/resources/initial-setup-window.ui:10
msgid "Polari Setup"
msgstr "Configurazione di Polari"

#: data/resources/initial-setup-window.ui:60
msgid "Not connected"
msgstr "Non connessi"

#: data/resources/initial-setup-window.ui:73
msgid "Please connect to the internet to continue the setup."
msgstr "Collegarsi a Internet per continuare la configurazione."

#: data/resources/initial-setup-window.ui:109
#: data/resources/initial-setup-window.ui:159
msgid "Welcome to Polari"
msgstr "Benvenuti in Polari"

#: data/resources/initial-setup-window.ui:121
msgid ""
"Polari is an easy way to chat using IRC. Select a network to get started."
msgstr ""
"Polari è il modo più semplice per conversare utilizzando IRC. Per iniziare, "
"selezionare una rete."

#: data/resources/initial-setup-window.ui:171
msgid ""
"Select rooms you want to connect to. You can add more networks and rooms "
"later, by clicking the + button."
msgstr ""
"Selezionare le stanze a cui collegarsi. Facendo clic sul pulsante + è "
"possibile aggiungere nuove reti e stanze."

#: data/resources/join-room-dialog.ui:4 src/joinDialog.js:232
msgid "Join Chat Room"
msgstr "Entra in stanza di conversazione"

#: data/resources/join-room-dialog.ui:19
msgid "_Join"
msgstr "_Entra"

#: data/resources/join-room-dialog.ui:46
msgid "C_onnection"
msgstr "C_onnessione"

#: data/resources/join-room-dialog.ui:75
msgid "_Add Network"
msgstr "A_ggiungi rete"

#: data/resources/join-room-dialog.ui:159
msgid "_Add"
msgstr "A_ggiungi"

#: data/resources/join-room-dialog.ui:178
msgid "_Custom Network"
msgstr "Rete personali_zzata"

#: data/resources/main-window.ui:22
msgid "Keyboard Shortcuts"
msgstr "Scorciatoie da tastiera"

#: data/resources/main-window.ui:26
msgid "Help"
msgstr "Aiuto"

#: data/resources/main-window.ui:30
msgid "About"
msgstr "Informazioni"

#: data/resources/main-window.ui:34 data/resources/menus.ui:6
#: src/application.js:58
msgid "Quit"
msgstr "Esci"

#: data/resources/main-window.ui:55 data/resources/main-window.ui:69
msgid "Add rooms and networks"
msgstr "Aggiunge stanze e reti"

#: data/resources/main-window.ui:197
msgid "Show connected users"
msgstr "Mostra utenti connessi"

#: data/resources/main-window.ui:225 src/roomList.js:474 src/userList.js:417
msgid "Offline"
msgstr "Offline"

#: data/resources/main-window.ui:234
msgid "Go online to chat and receive messages."
msgstr "Andare online per chiacchierare e ricevere messaggi."

#: data/resources/nick-popover.ui:16
msgid "Change nickname:"
msgstr "Cambia soprannome:"

#: data/resources/nick-popover.ui:27
msgid "_Change"
msgstr "C_ambia"

#: data/resources/room-list-header.ui:145
msgid "Connect"
msgstr "Connetti"

#: data/resources/room-list-header.ui:152
msgid "Reconnect"
msgstr "Riconnetti"

#: data/resources/room-list-header.ui:159
msgid "Disconnect"
msgstr "Disconnetti"

#: data/resources/room-list-header.ui:166
msgid "Remove"
msgstr "Rimuovi"

#: data/resources/room-list-header.ui:173
msgid "Properties"
msgstr "Proprietà"

#: data/resources/server-room-list.ui:45
msgid "Enter room name to add"
msgstr "Inserire il nome della stanza da aggiungere"

#: data/resources/user-details.ui:27
msgid "Loading details"
msgstr "Caricamento dettagli"

#: data/resources/user-details.ui:60
msgid "Last Activity:"
msgstr "Ultima attività:"

#: data/resources/user-details.ui:159
msgid "Will notify if user appears online."
msgstr "Invia una notifica se l'utente appare online."

#: data/resources/user-details.ui:181
msgid "Start Conversation"
msgstr "Inizia conversazione"

#: src/application.js:44
msgid "Start Telepathy client"
msgstr "Avvia client Telepathy"

#: src/application.js:49
msgid "Start in debug mode"
msgstr "Avvia in modalità debug"

#: src/application.js:52
msgid "Allow running alongside another instance"
msgstr "Consente l'esecuzione insieme a un'altra istanza"

#: src/application.js:55
msgid "Print version and exit"
msgstr "Stampa la versione ed esce"

#: src/application.js:486 src/utils.js:241
msgid "Failed to open link"
msgstr "Apertura del collegamento non riuscita"

#: src/application.js:761
#, javascript-format
msgid "%s removed."
msgstr "%s rimosso."

#: src/application.js:840
msgid "translator-credits"
msgstr "Milo Casagrande <milo@milo.name>"

#: src/application.js:846
msgid "Learn more about Polari"
msgstr "Maggiori informazioni su Polari"

#: src/appNotifications.js:85
msgid "Undo"
msgstr "Annulla"

#: src/chatView.js:138
msgid "New Messages"
msgstr "Nuovi messaggi"

#: src/chatView.js:786
msgid "Open Link"
msgstr "Apri collegamento"

#: src/chatView.js:792
msgid "Copy Link Address"
msgstr "Copia indirizzo collegamento"

#: src/chatView.js:964
#, javascript-format
msgid "%s is now known as %s"
msgstr "L'utente %s è ora conosciuto come %s"

#: src/chatView.js:969
#, javascript-format
msgid "%s has disconnected"
msgstr "L'utente %s si è disconnesso"

#: src/chatView.js:978
#, javascript-format
msgid "%s has been kicked by %s"
msgstr "L'utente %s è stato cacciato (kick) da %s"

#: src/chatView.js:979
#, javascript-format
msgid "%s has been kicked"
msgstr "L'utente %s è stato cacciato (kick)"

#: src/chatView.js:986
#, javascript-format
msgid "%s has been banned by %s"
msgstr "L'utente %s è stato bandito da %s"

#: src/chatView.js:987
#, javascript-format
msgid "%s has been banned"
msgstr "L'utente %s è stato bandito"

#: src/chatView.js:992
#, javascript-format
msgid "%s joined"
msgstr "L'utente %s è entrato"

#: src/chatView.js:997
#, javascript-format
msgid "%s left"
msgstr "L'utente %s se n'è andato"

#: src/chatView.js:1094
#, javascript-format
msgid "%d user joined"
msgid_plural "%d users joined"
msgstr[0] "%d utente è entrato"
msgstr[1] "%d utenti sono entrati"

#: src/chatView.js:1101
#, javascript-format
msgid "%d user left"
msgid_plural "%d users left"
msgstr[0] "%d utente se n'è andato"
msgstr[1] "%d utenti se ne sono andati"

#. today
#. Translators: Time in 24h format
#: src/chatView.js:1173
msgid "%H∶%M"
msgstr "%H∶%M"

#. yesterday
#. Translators: this is the word "Yesterday" followed by a
#. time string in 24h format. i.e. "Yesterday, 14:30"
#: src/chatView.js:1178
#, no-c-format
msgid "Yesterday, %H∶%M"
msgstr "Ieri, %H:%M"

#. this week
#. Translators: this is the week day name followed by a time
#. string in 24h format. i.e. "Monday, 14:30"
#: src/chatView.js:1183
#, no-c-format
msgid "%A, %H∶%M"
msgstr "%A, %H∶%M"

#. this year
#. Translators: this is the month name and day number
#. followed by a time string in 24h format.
#. i.e. "May 25, 14:30"
#: src/chatView.js:1189
#, no-c-format
msgid "%B %d, %H∶%M"
msgstr "%d %B, %H∶%M"

#. before this year
#. Translators: this is the month name, day number, year
#. number followed by a time string in 24h format.
#. i.e. "May 25 2012, 14:30"
#: src/chatView.js:1195
#, no-c-format
msgid "%B %d %Y, %H∶%M"
msgstr "%d %B %Y, %H∶%M"

#. eslint-disable-next-line no-lonely-if
#. today
#. Translators: Time in 12h format
#: src/chatView.js:1201
msgid "%l∶%M %p"
msgstr "%I∶%M %p"

#. yesterday
#. Translators: this is the word "Yesterday" followed by a
#. time string in 12h format. i.e. "Yesterday, 2:30 pm"
#: src/chatView.js:1206
#, no-c-format
msgid "Yesterday, %l∶%M %p"
msgstr "Ieri, %I∶%M %p"

#. this week
#. Translators: this is the week day name followed by a time
#. string in 12h format. i.e. "Monday, 2:30 pm"
#: src/chatView.js:1211
#, no-c-format
msgid "%A, %l∶%M %p"
msgstr "%A, %I∶%M %p"

#. this year
#. Translators: this is the month name and day number
#. followed by a time string in 12h format.
#. i.e. "May 25, 2:30 pm"
#: src/chatView.js:1217
#, no-c-format
msgid "%B %d, %l∶%M %p"
msgstr "%d %B, %I∶%M %p"

#. before this year
#. Translators: this is the month name, day number, year
#. number followed by a time string in 12h format.
#. i.e. "May 25 2012, 2:30 pm"
#: src/chatView.js:1223
#, no-c-format
msgid "%B %d %Y, %l∶%M %p"
msgstr "%d %B %Y, %I∶%M %p"

#: src/connections.js:42
msgid "Already added"
msgstr "Già aggiunto"

#: src/connections.js:104
msgid "No results."
msgstr "Nessun risultato."

#: src/connections.js:511
#, javascript-format
msgid "“%s” Properties"
msgstr "Proprietà di «%s»"

#: src/connections.js:555
msgid ""
"Polari disconnected due to a network error. Please check if the address "
"field is correct."
msgstr ""
"Polari si è scollegato a causa di un errore di rete. Verificare che "
"l'indirizzo sia corretto."

#: src/entryArea.js:370
#, javascript-format
msgid "Paste %s line of text to public paste service?"
msgid_plural "Paste %s lines of text to public paste service?"
msgstr[0] "Incollare %s riga di testo nel servizio di «paste» pubblico?"
msgstr[1] "Incollare %s righe di testo nel servizio di «paste» pubblico?"

#: src/entryArea.js:374
#, javascript-format
msgid "Uploading %s line of text to public paste service…"
msgid_plural "Uploading %s lines of text to public paste service…"
msgstr[0] "Caricamento di %s riga di testo al servizio di «paste» pubblico…"
msgstr[1] "Caricamento di %s righe di testo al servizio di «paste» pubblico…"

#: src/entryArea.js:381
msgid "Upload image to public paste service?"
msgstr "Caricare l'immagine nel servizio di «paste» pubblico?"

#: src/entryArea.js:382
msgid "Uploading image to public paste service…"
msgstr "Caricamento dell'immagine nel servizio di «paste» pubblico…"

#. Translators: %s is a filename
#: src/entryArea.js:404
#, javascript-format
msgid "Upload “%s” to public paste service?"
msgstr "Caricare «%s» nel servizio di «paste» pubblico?"

#. Translators: %s is a filename
#: src/entryArea.js:406
#, javascript-format
msgid "Uploading “%s” to public paste service…"
msgstr "Caricamento di «%s» nel servizio di «paste» pubblico…"

#. translators: %s is a nick, #%s a channel
#: src/entryArea.js:415
#, javascript-format
msgid "%s in #%s"
msgstr "%s in #%s"

#: src/entryArea.js:417
#, javascript-format
msgid "Paste from %s"
msgstr "Incolla da %s"

#: src/initialSetup.js:85
msgid "_Back"
msgstr "_Indietro"

#: src/initialSetup.js:86
msgid "_Done"
msgstr "Fa_tto"

#: src/initialSetup.js:86
msgid "_Next"
msgstr "_Successivo"

#. commands that would be nice to support:
#.
#. AWAY: N_("/AWAY [<message>] — sets or unsets away message"),
#. LIST: N_("/LIST [<channel>] — lists stats on <channel>, or all channels on the server"),
#. MODE: "/MODE <mode> <nick|channel> — ",
#. NOTICE: N_("/NOTICE <nick|channel> <message> — sends notice to <nick|channel>"),
#. OP: N_("/OP <nick> — gives channel operator status to <nick>"),
#.
#.
#: src/ircParser.js:24
msgid ""
"/CLOSE [<channel>] [<reason>] — closes <channel>, by default the current one"
msgstr ""
"/CLOSE [<canale>] [<motivo>] — Chiude il <canale>, in modo predefinito "
"quello corrente"

#: src/ircParser.js:25
msgid ""
"/HELP [<command>] — displays help for <command>, or a list of available "
"commands"
msgstr ""
"/HELP [<comando>] — Visualizza l'aiuto per <comando> o un elenco di comandi "
"disponibili"

#: src/ircParser.js:26
msgid ""
"/INVITE <nick> [<channel>] — invites <nick> to <channel>, or the current one"
msgstr ""
"/INVITE <soprannome> [<canale>] — Invita <soprannome> in <canale> o nel "
"canale corrente"

#: src/ircParser.js:27
msgid "/JOIN <channel> — joins <channel>"
msgstr "/JOIN <canale> — Entra in <canale>"

#: src/ircParser.js:28
msgid "/KICK <nick> — kicks <nick> from current channel"
msgstr "/KICK <soprannome> — Caccia <soprannome> dal canale corrente"

#: src/ircParser.js:29
msgid "/ME <action> — sends <action> to the current channel"
msgstr "/ME <azione> — Invia <azione> al canale corrente"

#: src/ircParser.js:30
msgid "/MSG <nick> [<message>] — sends a private message to <nick>"
msgstr ""
"/MSG <soprannome> [<messaggio>] — Invia un messaggio privato a <soprannome>"

#: src/ircParser.js:31
msgid "/NAMES — lists users on the current channel"
msgstr "/NAMES — Elenca gli utenti presenti nel canale corrente"

#: src/ircParser.js:32
msgid "/NICK <nickname> — sets your nick to <nickname>"
msgstr "/NICK <soprannome> — Imposta il soprannome a <soprannome>"

#: src/ircParser.js:33
msgid ""
"/PART [<channel>] [<reason>] — leaves <channel>, by default the current one"
msgstr ""
"/PART [<canale>] [<motivo>] — Esce dal <canale>, in modo predefinito quello "
"corrente"

#: src/ircParser.js:34
msgid "/QUERY <nick> — opens a private conversation with <nick>"
msgstr "/QUERY <soprannome> — Apre una conversazione privata con <soprannome>"

#: src/ircParser.js:35
msgid "/QUIT [<reason>] — disconnects from the current server"
msgstr "/QUIT [<motivo>] — Disconnette dal server corrente"

#: src/ircParser.js:36
msgid "/SAY <text> — sends <text> to the current room/contact"
msgstr "/SAY <testo> — Invia <testo> nella stanza o al contatto corrente"

#: src/ircParser.js:37
msgid "/TOPIC <topic> — sets the topic to <topic>, or shows the current one"
msgstr ""
"/TOPIC <argomento> — Imposta l'argomento ad <argomento> oppure mostra quello "
"corrente"

#: src/ircParser.js:38
msgid "/WHOIS <nick> — requests information on <nick>"
msgstr "/WHOIS <soprannome> — Richiede informazioni riguardo a <soprannome>"

#: src/ircParser.js:41
msgid "Unknown command — try /HELP for a list of available commands"
msgstr ""
"Comando sconosciuto — Provare /HELP per l'elenco dei comandi disponibili"

#: src/ircParser.js:55
#, javascript-format
msgid "Usage: %s"
msgstr "Uso: %s"

#: src/ircParser.js:92
msgid "Known commands:"
msgstr "Comandi noti:"

#: src/ircParser.js:194
#, javascript-format
msgid "Users on %s:"
msgstr "Utenti in %s:"

#: src/ircParser.js:273
msgid "No topic set"
msgstr "Nessun argomento impostato"

#: src/ircParser.js:312
#, javascript-format
msgid "User: %s - Last activity: %s"
msgstr "Utente: %s - Ultima attività: %s"

#: src/joinDialog.js:232
msgid "Add Network"
msgstr "Aggiungi rete"

#: src/mainWindow.js:399
#, javascript-format
msgid "%d user"
msgid_plural "%d users"
msgstr[0] "%d utente"
msgstr[1] "%d utenti"

#: src/roomList.js:253
msgid "Leave chatroom"
msgstr "Esci dalla stanza"

#: src/roomList.js:253
msgid "End conversation"
msgstr "Termina conversazione"

#: src/roomList.js:269
msgid "Unmute"
msgstr "Abilita"

#: src/roomList.js:273
msgid "Mute"
msgstr "Zittisci"

#: src/roomList.js:375
#, javascript-format
msgid "Network %s has an error"
msgstr "La rete %s presenta un errore"

#. Translators: This is an account name followed by a
#. server address, e.g. "GNOME (irc.gnome.org)"
#: src/roomList.js:446
#, javascript-format
msgid "%s (%s)"
msgstr "%s (%s)"

#: src/roomList.js:453
msgid "Connection Problem"
msgstr "Errore di connessione"

#: src/roomList.js:470
msgid "Connected"
msgstr "Connessi"

#: src/roomList.js:472
msgid "Connecting…"
msgstr "Connessione…"

#: src/roomList.js:476
msgid "Unknown"
msgstr "Sconosiuto"

#: src/roomList.js:496
#, javascript-format
msgid "Could not connect to %s in a safe way."
msgstr "Impossibile stabilire una connessione con %s in modo sicuro."

#: src/roomList.js:499
#, javascript-format
msgid "%s requires a password."
msgstr "%s richiede una password."

#: src/roomList.js:505
#, javascript-format
msgid "Could not connect to %s. The server is busy."
msgstr "Impossibile stabilire una connessione con %s: il server è occupato."

#: src/roomList.js:508
#, javascript-format
msgid "Could not connect to %s."
msgstr "Impossibile stabilire una connessione con %s."

#: src/roomStack.js:100
msgid "Should the password be saved?"
msgstr "Salvare la password?"

#: src/roomStack.js:102 src/telepathyClient.js:668
#, javascript-format
msgid ""
"Identification will happen automatically the next time you connect to %s"
msgstr ""
"L'identificazione avverrà automaticamente alla successiva connessione a %s"

#: src/roomStack.js:108
msgid "_Save Password"
msgstr "Salva pass_word"

#: src/roomStack.js:141
msgid "Failed to join the room"
msgstr "Impossibile entrare nella stanza"

#: src/roomStack.js:143
msgid "_Retry"
msgstr "_Riprova"

#: src/roomStack.js:167
msgid "The room is full."
msgstr "La stanza è piena."

#: src/roomStack.js:170
msgid "You have been banned from the room."
msgstr "Si è stati banditi dalla stanza."

#: src/roomStack.js:173
msgid "The room is invite-only."
msgstr "La stanza è accessibile solo su invito."

#: src/roomStack.js:176
msgid "You have been kicked from the room."
msgstr "Si è stati cacciati dalla stanza."

#: src/roomStack.js:179
msgid "It is not possible to join the room now, but you can retry later."
msgstr ""
"Non è possibile entrare nella stanza in questo momento, ma è possibile "
"riprovare più tardi."

#: src/roomStack.js:211
msgid "Join a room using the + button."
msgstr "Entrare in una stanza utilizzando il pulsante +"

#: src/telepathyClient.js:477
msgid "Good Bye"
msgstr "Ciao!"

#. Translators: Those are a botname and an accountName, e.g.
#. "Save NickServ password for GNOME"
#: src/telepathyClient.js:667
#, javascript-format
msgid "Save %s password for %s?"
msgstr "Salvare la password di %s per %s?"

#: src/telepathyClient.js:671
msgid "Save"
msgstr "Salva"

#. Translators: This is the title of the notification announcing a newly
#. received message, in the form "user-nickname in room-display-name"
#: src/telepathyClient.js:707
#, javascript-format
msgid "%s in %s"
msgstr "%s in %s"

#: src/userList.js:413
msgid "Available in another room."
msgstr "Disponibile in un'altra stanza."

#: src/userList.js:415
msgid "Online"
msgstr "Online"

#: src/userList.js:578
msgid "No Results"
msgstr "Nessun risultato"

#: src/userTracker.js:369
msgid "User is online"
msgstr "L'utente è online"

#: src/userTracker.js:370
#, javascript-format
msgid "User %s is now online."
msgstr "L'utente %s è ora online."

#: src/utils.js:137
#, javascript-format
msgid "Polari server password for %s"
msgstr "Password del server per %s"

#: src/utils.js:142
#, javascript-format
msgid "Polari NickServ password for %s"
msgstr "Password del NickServ per %s"

#: src/utils.js:372
msgid "Now"
msgstr "Ora"

#: src/utils.js:375
msgid "Unavailable"
msgstr "Non disponibile"

#: src/utils.js:379
#, javascript-format
msgid "%d second ago"
msgid_plural "%d seconds ago"
msgstr[0] "%d secondo fa"
msgstr[1] "%d secondi fa"

#: src/utils.js:386
#, javascript-format
msgid "%d minute ago"
msgid_plural "%d minutes ago"
msgstr[0] "%d minuto fa"
msgstr[1] "%d minuti fa"

#: src/utils.js:393
#, javascript-format
msgid "%d hour ago"
msgid_plural "%d hours ago"
msgstr[0] "%d ora fa"
msgstr[1] "%d ore fa"

#: src/utils.js:400
#, javascript-format
msgid "%d day ago"
msgid_plural "%d days ago"
msgstr[0] "%d giorno fa"
msgstr[1] "%d giorni fa"

#: src/utils.js:407
#, javascript-format
msgid "%d week ago"
msgid_plural "%d weeks ago"
msgstr[0] "%d settimana fa"
msgstr[1] "%d settimane fa"

#: src/utils.js:413
#, javascript-format
msgid "%d month ago"
msgid_plural "%d months ago"
msgstr[0] "%d mese fa"
msgstr[1] "%d mesi fa"
